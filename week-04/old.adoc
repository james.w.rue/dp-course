
=== SEIS664-20YY-MM team
You all are now members, or will be members of a Gitlab team named SEIS664-20YY-MM where YY is the year and MM is the month the class started (either 02 for spring or 09 for fall.)

You have privileges to create issues, forks, and pull requests on both dp-course and node-svc. 



== Gitlab issues, branches, and pull requests

Here is an overview of the following sections: The following image is for dp-course. 

image:overview.png[]

Now that we have discussed the overall vision for the course repositories, we are going to start using Gitlab issues to communicate more, instead of relying on Teams for everything. If you think you have discovered a problem in a lab, you should file a Gitlab issue. To finish this lab, follow this example, or preferably come up with your own improvement to the course materials. 

Here are the steps for filing a Gitlab issue, and fixing it via a branch and pull request. 

Something that I thought of last night (2020-10-09) was that we never really talk about nano. I want to put a link into the lab 00 instructions so people can at least access basic how-tos. 

For the purposes of this exercise, I am not just going to fix it directly. I am going to raise an issue, assign it to myself, fix it, and submit a pull request to ... me!

In other words, I am operating first as "CharlesTBetz-test" which is a Gitlab account I created that has no more rights than any of you. Acting as such, I have cloned dm-academy/dp-course down to my Google Cloud Shell, and I am only logged into my ssh agent with the private key associated with CharlesTBetz-test. 

So, how do I let the project know I think we should provide a pointer to nano instructions? Rather than making a technical change, I am first going to communicate that we have a problem. 

This is an important step now that we are operating as a team. What I think is a problem, might not be. Someone else might already be working on it. So it is very important to communicate before starting technical work. 

=== Create an issue 
First, I go to 

https://gitlab.com/CharlesTBetz/dp-course,

click on "Issues"  

image:issues.png[]

and then the "New Issue" button that appears on the right. 

image:issueBtn.png[]

I document my issue:

image:nanoIssue.png[]

(Note that the issue appears as a word bubble coming from my icon as CharlesTBetz-test.)

I see on the right I can assign an owner ("Assignees"):

image:assignees.png[]. 

I click "Submit new issue." 

=== Assign it

Since I (as CharlesTBetz-test) belong to the dm-academy/node-svc repo with sufficient privileges, I can assign the issue to myself. 

image:providersIssue.png[]

I click on Assign Myself, and I am assigned. 

image:assignees.png[]

I click on Submit New Issue: 

image:submit.png[]

and the issue is created. It was given #44, this will be useful below.

It is also possible to create an issue directly out of Teams, using "chatops" techniques. We will cover this subsequently. 

=== Create a branch

NOTE: We've been using Gitlab for the Open Group which allows the immediate creation of a branch on creation of an issue. Gitlab doesn't do this, which I see as a disadvantage. 

Now that I've been assigned the issue, I am going to create a local branch. I could do this on the website as well. 

I leave the Gitlab UI and go to my Google Cloud Shell, where in the repos/dp-course direcgory I type: 

[source, bash]
----
`$ git checkout -b nano-fix`
Switched to a new branch 'nano-fix'
----

I could also have created the branch first via `git branch` and then checked it out. The -b flag lets me do both. I also could have created the branch in the Gitlab GUI. 

Now, I can fix the issue. 

`$ nano week-00/00-tech-lab.adoc`

image:nanoFix.png[]

=== Commit, push, and create a pull request

Now, it's time to commit, push, and create a pull request: 

[source, bash]
----
$ git add . -A
$ git commit -m "fixes nano issue #44"
[provider-fix 8d1ed0a] fixes issue #44
 1 file changed, 1 insertion(+), 1 deletion(-)
$ git push origin master
Everything up-to-date
----
Oops, that was a mistake. We need to git push origin <branch name>.

[source,bash]
----
2019-MBA:dp-course char$ git push origin nano-fix
Enumerating objects: 16, done.
Counting objects: 100% (16/16), done.
Delta compression using up to 4 threads
Compressing objects: 100% (10/10), done.
Writing objects: 100% (10/10), 655.71 KiB | 15.61 MiB/s, done.
Total 10 (delta 3), reused 0 (delta 0)
remote: Resolving deltas: 100% (3/3), completed with 3 local objects.
remote: 
remote: Create a pull request for 'nano-fix' on Gitlab by visiting:
remote:      https://gitlab.com/CharlesTBetz/dp-course/pull/new/nano-fix
remote: 
To Gitlab.com:dm-academy/dp-course.git
 * [new branch]      nano-fix -> nano-fix
2019-MBA:dp-course char$ 
----

Now, if I go to Gitlab and look at the 00-lab file:

image:oldLab.png[]

it's not fixed. 

BUT ... look at the branches. Aha, I was still on master: 

image:branches.png[]

Switch to "nano-fix" and the new material is there: 

image:fixed.png[]

But how do I get it into master? I need to issue a *pull request* to me as the maintainer. Fortunately, if I look above at my console output when I did the git push -- very nice, Gitlab has given us a very cool link to go and create a pull request (). I paste it into my browser: 

https://gitlab.com/CharlesTBetz/dp-course/pull/new/nano-fix

and fill it out thus, requesting a review on the right (from me as maintainer): 

image:pr.png[]

I switch to my maintainer account and approve and merge it. The issue can also be now closed. 

NOTE: What happens if, while your pull request is waiting to be merged, a different pull request that modifies some of the same lines of code is merged? You'll get a merge conflict. A merge conflict is when git can't reconcile the differences between what you're asking to merge and what someone else recently changed in the same file. You'll get a screen in Gitlab that looks like this:
image:merge_conflict.jpg[]
In this case, you'll need to decide which changes should remain in the branch you're merging to.

== Setting upstream remote for node-svc

Up until now, we have been somewhat haphazard and basic with our usage of git and Gitlab, just as a startup with a couple of individuals might be -- primarily using it as a repository/archive, but not very effectively as a collaboration tool. 

In particular we have been using this basic approach: 

image:../week-00/Gitlab-arch.png[]

The problem with this approach is that there is no convenient way to pull updates from upstream into our local repo. 

There are two ways we can solve this. 

1. I could make you all members of my Gitlab project and you could clone it directly. However, then you cannot run Gitlab Actions yourself.
2. We can continue to have you fork and then clone (as you would if you were participating in an open source project for which you were not yet a committer) but set things up so that you can do it all from the command line. 

We will go with option #2. 

In order to proceed with #2, we need to better understand the concept of a git "remote." Git is a powerful tool for distributed collaboration on complex software projects, and therefore itself is complex. 

According to https://www.git-tower.com/, "A remote in Git is a common repository that all team members use to exchange their changes. In most cases, such a remote repository is stored on a code hosting service like Gitlab or on an internal server."

What this does not mention is that a git repository can have _multiple_ remotes. Your local clone on the Google Cloud Shell has, as a remote, the fork you created in Gitlab. This is its origin.  Now, you are going to set as your _upstream_ remote the original source repository you cloned from as a template. 

(This lab is based on these instructions: https://docs.Gitlab.com/en/free-pro-team@latest/Gitlab/collaborating-with-issues-and-pull-requests/configuring-a-remote-for-a-fork)

So, let's configure a remote for node-svc. 

Go to your `repos` directory (or wherever you have put the repos). Enter (`cd`) into the node-svc repo, if you aren't there already from the last step. 

At the command prompt, enter: 

`git remote add upstream https://gitlab.com/CharlesTBetz/node-svc`

This adds the original repo as the `upstream` remote. You still have the `origin remote`, under _your_ Gitlab account. 

I have done so using a test Gitlab account called CharlesTBetz-test. I can see the result thus: 

[source,bash]
----
betz4871:node-svc$ git remote -v
origin  git@Gitlab.com:CharlesTBetz-test/node-svc.git (fetch)
origin  git@Gitlab.com:CharlesTBetz-test/node-svc.git (push)
upstream        https://gitlab.com/CharlesTBetz/node-svc (fetch)
upstream        https://gitlab.com/CharlesTBetz/node-svc (push)
betz4871:node-svc$
----

Notice there is both an `origin` and `upstream` remote. Examine the URLs for each. One points to the dm-academy master repo, the other points to its forked replica on my test account. 

Let's synch things up: 

`git pull upstream master`

Unless you have changed your local copy, or upstream has changed since you forked and cloned, this should indicate that everything is up to date. 

The overall architecture now looks like this (notice 6 and 7):

image:Gitlab-v2.png[]

From now on, before pushing any changes and especially before issuing a pull request, be sure to synch your local copy with upstream. 

IMPORTANT: It is convenient to sync your online git repo with upstream by just pushing the changes from your local repo (7 + 4). You also can compare across upstream and your fork and merge changes directly on Gitlab (8 + 5). 

Here is the final configuration you should have: 

image:finalRepo.png[]

If the instructor indicates that a change from dm-academy/node-svc needs to be pulled into your local version, execute the following: 

[source,bash]
----
$ git fetch upstream  # pull down changes from dm-academy/node-svc
$ git reset --hard upstream/master   # or perhaps a different branch, e.g. /03
$ git push --force  # update your copy on Gitlab
----

Notice that this will discard any changes you have made locally. 

=== Assignment

Assignment: Using what you have learned above, find some small BUT REAL thing to fix in either dp-course or node-svc. It can be as simple as fixing a mis-spelling or grammar, or adding some clarifying language. If you truly cannot find anything to improve, just subsitute a synonym for some work in dp-course. 

Optionally, you may also raise an issue, branch, and pull request on the https://gitlab.com/theopengroup/dpbok-community-edition[community edition of the DPBoK]. I will grant extra credit for this. 

In the assignment submission on Canvas, paste the link to the pull request (PR) for the change you made. 
